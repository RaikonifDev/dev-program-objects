1. Write unit tests in `tests` folder
   1. Unit tests in raw format are detailed in notebooks
   2. Unit tests must reference `src` modules
2. Add `.gitignore` file (do not forget to exclude `virtualenvs` folder)
3. [must] In this  `Readme.md` Add steps to manage 3rd party packages (if there's any)
4. [must] In this  `Readme.md` Add steps to launch venv and run all `unit tests`
   1. [nice to have] Add steps to run unit tests with  `tox`
5. [must] once done delete lines 1-8